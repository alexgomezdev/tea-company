import React from "react";
import "../styles/download.scss";
import { StaticImage } from "gatsby-plugin-image";


export const Download = () => {
  return (
    <div id="download" className="download bg-gradient-my-good-app">
      <div className="container">
        <div className="row">
          <div className="col-lg-5 d-flex align-items-center">
            <div className="my-5 mx-auto mx-lg-0 text-lg-start text-center">
              <h2 className="text-uppercase text-white mb-4">
                Lorem ipsum dolor <br />
                sit amet
              </h2>
              <div className="btn-area">
                <a href={"http://www.google.com"} className="btn btn-dark me-3">
                  Google Play
                </a>
                <a href={"http://www.google.com"} className="btn btn-dark">
                  App Store
                </a>
              </div>
            </div>
          </div>
          <div className="col-lg-7">
            <StaticImage className="img-fluid phone" src={"../images/18_1024x1024@2x.png"} alt="" />
          </div>
        </div>
      </div>
    </div>
  );
};
