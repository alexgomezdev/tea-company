import React from "react";
import File from "../images/file.png"
import Drugs from "../images/drugs.png"
import Checklist from "../images/to-do-list.png"
import Catalog from "../images/catalogo.png"
import Calender from "../images/Calendar.png"
import { StaticImage } from "gatsby-plugin-image";


import "../styles/circle.scss"
import Fade from "react-reveal/Fade";
import Zoom from "react-reveal/Zoom";

export const Circle = () => {
    return (
        <div className="circle bg-contrast py-1 pt-lg-5 pb-lg-1">
            <div className="container py-1 pt-lg-5 pb-lg-1">
                <div className="square">
                    <div className="outer-circle"/>
                    <div className="inner-circle"/>
                    <div className="content">
                        <Zoom>
                            <StaticImage src={"../images/Macha_Latte_Detail.png"} alt={"..."}/>
                        </Zoom>
                    </div>

                    <div className="ring">
                        <Fade down cascade>
                            <div className="dot">
                                <div className="dot-content">
                                    <img src={Catalog} className="dot-icon" alt={""}/>
                                    <span>Donec sit amet leo</span>
                                </div>
                            </div>
                            <div className="dot">
                                <div className="dot-content">
                                    <img src={Checklist} className="dot-icon" alt={""}/>
                                    <span>Donec sit amet leo</span>
                                </div>
                            </div>
                            <div className="dot">
                                <div className="dot-content">
                                    <img src={Calender} className="dot-icon" alt={""}/>
                                    <span>Donec sit amet leo</span>
                                    </div>
                            </div>
                            <div className="dot">
                                <div className="dot-content"><img src={Drugs} className="dot-icon" alt={""}/>
                                    <span>Donec sit amet leo</span>
                                </div>
                            </div>
                            <div className="dot">
                                <div className="dot-content">
                                    <img src={File} className="dot-icon" alt={""}/>
                                    <span>Donec sit amet leo</span>
                                </div>
                            </div>
                        </Fade>
                    </div>
                </div>
            </div>
        </div>
    )
}