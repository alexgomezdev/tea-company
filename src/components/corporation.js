import React from "react"
import Fade from 'react-reveal/Fade';

export const Corporation = () => (
    <div className="bg-light" style={{fontWeight: "lighter"}}>
        <Fade up>
            <div className="container py-5">
                <div className="row text-center text-lg-start">
                    <div className="col-lg-5 offset-lg-1 d-flex align-items-center justify-content-center justify-content-lg-start">
                        <h2 className="text-uppercase mb-lg-3">LOREM IPSUM</h2>
                    </div>
                    <div className="col-lg-5">
                        <p>
                        Ut venenatis vestibulum ante sit amet ullamcorper. Mauris quis convallis lorem. 
                        </p>
                        <p>
                        Suspendisse interdum diam ultricies mattis pellentesque.
                        </p>
                    </div>
                </div>
            </div>
        </Fade>
    </div>
);