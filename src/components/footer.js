import React from "react";
import "../styles/footer.scss";
import { Link } from "gatsby";
import { StaticImage } from "gatsby-plugin-image";


export const Footer = () => {
  return (
    <>
      <footer className="footer bg-white py-5">
        <div className="container">
          <div className="row align-items-center gx-5 gy-4">
            <div className="col-lg-3">
              <div className="mb-4 text-center">
                <StaticImage
                  src={"../images/green-cup-of-tea-seeklogo.com.png"}
                  className="img-fluid"
                  alt={"Logo"}
                  height={150}
                />
              </div>
              <div className="d-flex flex-row justify-content-center">
                <a
                  className="icon-circle link-text"
                  href="/#"
                >
                  <i className="bi bi-instagram" />
                </a>
                <a
                  className="icon-circle link-text"
                  href="/#"
                >
                  <i className="bi bi-facebook" />
                </a>
                {/*<a className="icon-circle link-text" href="https://twitter.com/">
                                    <i className="bi bi-twitter"/>
                                </a>*/}
                {/*<a className="icon-circle link-text" href="https://twitter.com/">
                                    <i className="bi bi-whatsapp"/>
    </a>*/}
                <a
                  className="icon-circle link-text"
                  href="/#"
                >
                  <i className="bi bi-youtube" />
                </a>
                <a
                  className="icon-circle link-text"
                  href="/#"
                >
                  <i className="fab fa-tiktok" />
                </a>
                <a className="icon-circle link-text" href="/#">
                    <i className="bi bi-twitch"/>
                </a>
                <a className="icon-circle link-text" href="/#">
                    <i className="fab fa-pinterest"/>
                </a>
              </div>
            </div>
            <div className="col-lg-3">
              <nav className="nav flex-column text-center text-lg-start">
                <Link className="nav-link link-dark " to="/#">
                  Order your tea
                </Link>
                <Link className="nav-link link-dark" to="/#">
                  Sign up
                </Link>
                <Link className="nav-link link-dark" to="/#">
                  Login
                </Link>
              </nav>
            </div>
            <div className="col-lg-3">
              <nav className="nav flex-column  text-center text-lg-start">
                <Link className="nav-link link-dark" to="/#plans">
                  Plans
                </Link>
                <Link className="nav-link link-dark" to="/#download">
                  Download
                </Link>
                
              </nav>
            </div>
            <div className="col-lg-3">
              <nav className="nav flex-column  text-center text-lg-start">
                <span className="nav-link link-dark">Contact:</span>
                <a
                  className="nav-link link-dark"
                  href="mailto:contact@tea.com"
                >
                  <strong>contact@tea.com</strong>
                </a>
              </nav>
            </div>
          </div>
        </div>
      </footer>
      <div className="bg-light py-3">
        <div className="container text-center d-flex flex-column flex-md-row justify-content-between">
          <span>TeaCompany - 2021</span>
          <span>
            <small>
              Made By{" "}
              <a
                href="https://alexgomez.dev/"
                target="_blank"
                rel="noreferrer"
              >
                AlexGomezDev
              </a>{" "}
              &amp; with <i className="bi bi-heart-fill text-danger" />
            </small>
          </span>
        </div>
      </div>
    </>
  );
};
