import React from "react";
import useWindowSize from "../hooks/use-window-size";
import "../styles/hero.scss";
import { Link } from "gatsby";
import { StaticImage } from "gatsby-plugin-image";

export const Hero = () => {
  const size = useWindowSize();

  return (
    <div
      className="hero bg-gradient-my-good-app  d-flex align-items-center"
      style={{ minHeight: `${size.height - 76}px` }}
    >
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-6">
            <h1 className="text-white">
              LOREM <br />
              <strong> IPSUM</strong>
            </h1>
            <h2 className="text-white">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            </h2>
            <div className="btn-area">
              <Link to="/" className="btn btn-white btn-lg">
                Register Now
              </Link>
            </div>
          </div>
          <div className="d-none d-lg-block col-lg-6 side-logo">
            <StaticImage src="../images//green-cup-of-tea-seeklogo.com.png"
              formats={["webp"]}
              layout="constrained"
              placeholder="none"
              transformOptions={{fit: "contain"}}
              height={400}
            />
          </div>
        </div>
      </div>
    </div>
  );
};
