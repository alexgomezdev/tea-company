import React from "react";
import Fade from "react-reveal/Fade";
import "../styles/promo.scss";
import { StaticImage } from "gatsby-plugin-image";

export const Promo = () => (
  <div className="promo bg-light">
    <Fade up>
      <div className="container">
        <h2 className="text-uppercase text-center mb-4">
          Lorem ipsum dolor sit amet
        </h2>
        <div className="row row-cols-1 row-cols-lg-3 g-3 g-lg-6">
          <div className="col">
            <div className="card h-100">
              <StaticImage
                src={"../images/greentea_large.jpg"}
                className="card-img-top img-promo"
                alt="..."
                height={250}
              />
              <div className="card-body">
                <p className="card-text">Mauris porttitor bibendum blandit.</p>
              </div>
            </div>
          </div>
          <div className="col">
            <div className="card h-100">
              <StaticImage
                src={"../images/greentea_large.jpg"}
                className="card-img-top img-promo"
                alt="..."
                height={250}
              />
              <div className="card-body">
                <p className="card-text">
                  Curabitur mauris nisl, iaculis non nisl ut, laoreet tristique
                  purus.
                </p>
              </div>
            </div>
          </div>
          <div className="col">
            <div className="card h-100">
              <StaticImage
                src={"../images/greentea_large.jpg"}
                className="card-img-top img-promo"
                alt="..."
                height={250}
              />
              <div className="card-body">
                <p className="card-text">
                  Etiam auctor laoreet arcu, a dictum arcu hendrerit id.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fade>
  </div>
);
