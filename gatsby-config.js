const path = require(`path`)

module.exports = {
    siteMetadata: {
        title: `MyGoodApp`,
        siteUrl: `https://www.mygoodapp.com`,
        description: `Blazing fast modern site generator for React`,
        keywords: "",
        logo: "/images/logo.png",
        icon: "/images/icon.png",
        author: "Conceptual Dynamic"
    },
    plugins: [
        "gatsby-plugin-sass",
        "gatsby-plugin-react-helmet",
        "gatsby-plugin-sitemap",
        {
            resolve: 'gatsby-plugin-web-font-loader',
            options: {
                google: {
                    families: ['Poppins:300,500,800']
                }
            }
        },
        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "images",
                path: path.join(__dirname, "src", "images"),
            },
        },
        {
            resolve: "gatsby-plugin-anchor-links",
        },
        `gatsby-plugin-image`,
        "gatsby-plugin-sharp",
        "gatsby-transformer-sharp",
    ],
};
